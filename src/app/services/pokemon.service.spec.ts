import { TestBed } from '@angular/core/testing';

import { pokemon } from './pokemon.service';

describe('GetPokemonService', () => {
  let service: pokemon;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(pokemon);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
