import { Component } from '@angular/core';
import { pokemon } from "../../services";
import { PokemonInformation } from 'src/app/model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  view = false;
  allPokemons: PokemonInformation[] = [];
  pokemonNum: number = 1010;
  constructor(
    public pokemon: pokemon
  ){}

  loadAllPokemon() {

    for (let i = 1; i <= this.pokemonNum; i++) {
      this.pokemon.getInformation(i.toString()).subscribe(
        (response: PokemonInformation) => {
          this.allPokemons.push(response);
         this.view = true;
        }
      )
    }
  }

  // getPokemon() {
  //   this.pokemon.getInformation(this.numPokemon)
  //   .subscribe(response => {
  //     this.imgPokemon = response.sprites.front_default;
  //     this.namePokemon = response.name;
  //     this.view = true;
  //   }, error => {
  //     alert(error);
  //   })
  // }
}
