import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router
  ){
    this.form = this.fb.group({
      Email: ['', Validators.required],
      Password: ['', Validators.required]
    })
  }

validData(){
  const User : User = {
    email: this.form.value.Email,
    password: this.form.value.Password
  }

  if (User.password != '' && User.email != '') {
      this.router.navigate(['/dashboard']);
    }
}

  ngOnInit(): void {
  }

}
