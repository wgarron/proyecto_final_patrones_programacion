import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Register } from 'src/app/model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router
  ){
    this.form = this.fb.group({
      Email: ['', Validators.required],
      Password: ['', Validators.required]
    })
  }

validData(){
  const User : Register = {
    email: this.form.value.Email,
    password: this.form.value.Password
  }

  if (User.password != '' && User.email != '') {
      this.router.navigate(['/dashboard']);
    }
}

  ngOnInit(): void {
  }

}
